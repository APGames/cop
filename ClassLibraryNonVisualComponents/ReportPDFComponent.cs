﻿using ClassLibraryNonVisualComponents.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.ComponentModel;
using System.IO;
using System.Linq;

namespace ClassLibraryNonVisualComponents
{
    public partial class ReportPDFComponent : Component
    {
        private ReportPDFTitle[] titles;

        public ReportPDFComponent()
        {
            InitializeComponent();
        }

        public ReportPDFComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public void SetTitles(params ReportPDFTitle[] titles)
        {
            this.titles = titles;
        }

        public void Report<T>(string fileOutputPath, params T[] data)
        {
            if (!File.Exists(fileOutputPath))
                throw new Exception("File is not exist");

            if (titles == null)
                throw new Exception("Titles is empty");

            using (Stream memoryStream = File.OpenWrite(fileOutputPath))
            {
                Document document = new Document(PageSize.A5, 10, 10, 10, 10);

                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                document.Open();

                var props = typeof(T).GetProperties();
                if (titles.Sum(x => x.toColumn - x.fromColumn + 1) != props.Length)
                    throw new Exception("Not match lengths titles and props");
                PdfPTable table = new PdfPTable(props.Length);
                for (int i = 0; i < titles.Length; i++)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(titles[i].value))
                    {
                        Colspan = titles[i].toColumn - titles[i].fromColumn + 1,
                        HorizontalAlignment = 1
                    };
                    table.AddCell(cell);
                }

                foreach (var item in data)
                {
                    foreach (var prop in props)
                    {
                        PdfPCell cell = new PdfPCell(new Phrase(prop.GetValue(item).ToString()));
                        table.AddCell(cell);
                    }
                }

                document.Add(table);

                document.Close();
                memoryStream.Close();
            }
        }
    }
}
