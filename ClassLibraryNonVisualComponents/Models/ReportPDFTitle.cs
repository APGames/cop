﻿namespace ClassLibraryNonVisualComponents.Models
{
    public struct ReportPDFTitle
    {
        public int fromColumn;
        public int toColumn;
        public string value;

        public ReportPDFTitle(int fromColumn, int toColumn, string value)
        {
            this.fromColumn = fromColumn;
            this.toColumn = toColumn;
            this.value = value;
        }
    }
}
