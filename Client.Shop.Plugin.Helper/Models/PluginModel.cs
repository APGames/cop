﻿namespace Client.Shop.PluginHelper.Models
{
    public class PluginModel
    {
        public ClientModel Client { get; set; }
        public ProductModel Product { get; set; }
    }
}
