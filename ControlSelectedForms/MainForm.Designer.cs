﻿namespace ControlSelectedForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonReset = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxPropA = new System.Windows.Forms.TextBox();
            this.textBoxPropB = new System.Windows.Forms.TextBox();
            this.textBoxPropC = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.treeViewOwn1 = new ClassLibraryTreeView.TreeViewOwn();
            this.dataTimePickerOwn = new ClassLibraryDataTimePicker.DataTimePickerOwn();
            this.controlListBoxSelected = new ClassLibraryControlSelected.ControlListBoxSelected();
            this.restoreComponent1 = new ClassLibraryNonVisualComponents.RestoreComponent(this.components);
            this.reportPDFComponent1 = new ClassLibraryNonVisualComponents.ReportPDFComponent(this.components);
            this.wordPieChartComponent1 = new ClassLibraryNonVisualComponents.WordPieChartComponent(this.components);
            this.button4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(12, 169);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(150, 23);
            this.buttonReset.TabIndex = 1;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(493, 496);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxPropA
            // 
            this.textBoxPropA.Location = new System.Drawing.Point(367, 469);
            this.textBoxPropA.Name = "textBoxPropA";
            this.textBoxPropA.Size = new System.Drawing.Size(63, 20);
            this.textBoxPropA.TabIndex = 5;
            // 
            // textBoxPropB
            // 
            this.textBoxPropB.Location = new System.Drawing.Point(436, 469);
            this.textBoxPropB.Name = "textBoxPropB";
            this.textBoxPropB.Size = new System.Drawing.Size(63, 20);
            this.textBoxPropB.TabIndex = 6;
            // 
            // textBoxPropC
            // 
            this.textBoxPropC.Location = new System.Drawing.Point(505, 469);
            this.textBoxPropC.Name = "textBoxPropC";
            this.textBoxPropC.Size = new System.Drawing.Size(63, 20);
            this.textBoxPropC.TabIndex = 7;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(87, 395);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "restore";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(87, 338);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 9;
            this.button3.Text = "report";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // treeViewOwn1
            // 
            this.treeViewOwn1.Location = new System.Drawing.Point(379, 12);
            this.treeViewOwn1.Name = "treeViewOwn1";
            this.treeViewOwn1.Size = new System.Drawing.Size(189, 451);
            this.treeViewOwn1.TabIndex = 3;
            // 
            // dataTimePickerOwn
            // 
            this.dataTimePickerOwn.FromDate = new System.DateTime(2020, 10, 4, 0, 0, 0, 0);
            this.dataTimePickerOwn.Location = new System.Drawing.Point(168, 12);
            this.dataTimePickerOwn.Name = "dataTimePickerOwn";
            this.dataTimePickerOwn.Size = new System.Drawing.Size(204, 47);
            this.dataTimePickerOwn.TabIndex = 2;
            this.dataTimePickerOwn.ToDate = new System.DateTime(2020, 12, 31, 0, 0, 0, 0);
            // 
            // controlListBoxSelected
            // 
            this.controlListBoxSelected.Location = new System.Drawing.Point(12, 12);
            this.controlListBoxSelected.Name = "controlListBoxSelected";
            this.controlListBoxSelected.SelectedIndex = 0;
            this.controlListBoxSelected.Size = new System.Drawing.Size(150, 151);
            this.controlListBoxSelected.TabIndex = 0;
            this.controlListBoxSelected.ComboBoxSelectedElementChange += new System.EventHandler(this.controlListBoxSelected_ComboBoxSelectedElementChange);
            // 
            // restoreComponent1
            // 
            this.restoreComponent1.FileName = "file";
            this.restoreComponent1.FileType = ClassLibraryNonVisualComponents.Enums.FileType.JSON;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(87, 439);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(101, 23);
            this.button4.TabIndex = 10;
            this.button4.Text = "pieChartSave";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 539);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBoxPropC);
            this.Controls.Add(this.textBoxPropB);
            this.Controls.Add(this.textBoxPropA);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.treeViewOwn1);
            this.Controls.Add(this.dataTimePickerOwn);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.controlListBoxSelected);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ClassLibraryControlSelected.ControlListBoxSelected controlListBoxSelected;
        private System.Windows.Forms.Button buttonReset;
        private ClassLibraryDataTimePicker.DataTimePickerOwn dataTimePickerOwn;
        private ClassLibraryTreeView.TreeViewOwn treeViewOwn1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxPropA;
        private System.Windows.Forms.TextBox textBoxPropB;
        private System.Windows.Forms.TextBox textBoxPropC;
        private ClassLibraryNonVisualComponents.RestoreComponent restoreComponent1;
        private System.Windows.Forms.Button button2;
        private ClassLibraryNonVisualComponents.ReportPDFComponent reportPDFComponent1;
        private System.Windows.Forms.Button button3;
        private ClassLibraryNonVisualComponents.WordPieChartComponent wordPieChartComponent1;
        private System.Windows.Forms.Button button4;
    }
}

