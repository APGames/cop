﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace ClassLibraryControlSelected
{
    public partial class ControlListBoxSelected : UserControl
    {
        private int _selectedIndex;
        private event EventHandler _listBoxSelectedElementChange;
        [Category("Спецификация"), Description("Порядковый номер выбранного элемента")]
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                if (value > -2 && value < checkedListBox.Items.Count)
                {
                    _selectedIndex = value;
                    checkedListBox.SelectedIndex = _selectedIndex;
                }
            }
        }
        [Category("Спецификация"), Description("Текст выбранной записи")]
        public string SelectedText
        {
            get { return checkedListBox.Text; }
        }
        [Category("Спецификация"), Description("Событие выбора элемента из списка")]
        public event EventHandler ComboBoxSelectedElementChange
        {
            add { _listBoxSelectedElementChange += value; }
            remove { _listBoxSelectedElementChange -= value; }
        }
        
        public ControlListBoxSelected()
        {
            InitializeComponent();
            checkedListBox.SelectedIndexChanged += (sender, e) => {
                _listBoxSelectedElementChange?.Invoke(sender, e);
            };
        }

        public void LoadData(params object[] objs)
        {
            foreach (var elem in objs)
            {
                checkedListBox.Items.Add(elem.ToString());
            }
        }
    }
}
