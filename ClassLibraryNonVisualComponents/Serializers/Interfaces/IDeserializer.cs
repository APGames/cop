﻿namespace ClassLibraryNonVisualComponents.Serializers.Interfaces
{
    public interface IDeserializer
    {
        T Deserialize<T>(string text);
    }
}
