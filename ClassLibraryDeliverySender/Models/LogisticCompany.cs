﻿using ClassLibraryDeliverySender.Abstract;
using ClassLibraryDeliverySender.Interfaces;
using System.Collections.Generic;

namespace ClassLibraryDeliverySender.Models
{
    public class LogisticCompany<T> : IDeliver<T>
    {
        public List<Deliver<T>> Delivers { get; set; }

        public void SendDeliver(T model)
        {
            foreach (var item in Delivers)
            {
                item.SendDeliver(model);
            }
        }
    }
}
