﻿using ClassLibraryPacker.Interfaces;

namespace ClassLibraryPacker.Abstract
{
    public abstract class BaseSendHandler<T> : ISendHandler<T>
    {
        private ISendHandler<T> next;

        public void SetNext(ISendHandler<T> packer)
        {
            next = packer;
        }

        public virtual void Send(T model)
        {
            next?.Send(model);
        }

        public abstract bool CanPack(T model);
    }
}
