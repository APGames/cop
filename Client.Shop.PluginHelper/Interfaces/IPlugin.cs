﻿using Client.Shop.PluginHelper.Models;
using System;

namespace Client.Shop.Plugin.Helper.Interfaces
{
    public interface IPlugin
    {
        string Name { get; }
        Version Version { get; }
        string Description { get; }
        string AuthorName { get; }

        void Execute(PluginModel model);
    }
}
