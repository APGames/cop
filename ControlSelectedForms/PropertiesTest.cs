﻿using ClassLibraryNonVisualComponents.Attributes;

namespace ControlSelectedForms
{
    [SerializeObject]
    public class PropertiesTest
    {
        public string PropA { get; set; }
        public string PropB { get; set; }
        public string PropC { get; set; }
    }
}
