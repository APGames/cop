﻿namespace ClassLibraryPacker.Interfaces
{
    public interface ISendHandler<T>
    {
        void Send(T model);
    }
}
