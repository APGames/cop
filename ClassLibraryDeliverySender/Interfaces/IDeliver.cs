﻿namespace ClassLibraryDeliverySender.Interfaces
{
    public interface IDeliver<T>
    {
        void SendDeliver(T model);
    }
}
