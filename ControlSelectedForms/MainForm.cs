﻿using System;
using System.Windows.Forms;
using ClassLibraryNonVisualComponents.Models;

namespace ControlSelectedForms
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            controlListBoxSelected.SelectedIndex = 0;
        }

        private void controlListBoxSelected_ComboBoxSelectedElementChange(object sender, EventArgs e)
        {
            MessageBox.Show(controlListBoxSelected.SelectedText);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            treeViewOwn1.AddInstance(new PropertiesTest
            {
                PropA = textBoxPropA.Text,
                PropB = textBoxPropB.Text,
                PropC = textBoxPropC.Text
            });
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            controlListBoxSelected.LoadData(new[] { "A", "B", "C" });
            treeViewOwn1.Configure(new[] { "PropA", "PropB", "PropC" });
            reportPDFComponent1.SetTitles(new ReportPDFTitle(0, 0, "A"), new ReportPDFTitle(1, 2, "B"));
            wordPieChartComponent1.SetTitles("A", "B", "C", "D", "E");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var data = restoreComponent1.GetData<PropertiesTest>("file.zip");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            reportPDFComponent1.Report("file.pdf", new[] {
                new PropertiesTest
                {
                    PropA = "A1",
                    PropB = "B1",
                    PropC = "C1"
                },
                new PropertiesTest
                {
                    PropA = "A2",
                    PropB = "B2",
                    PropC = "C2"
                },
            });
        }

        private void button4_Click(object sender, EventArgs e)
        {
            wordPieChartComponent1.Save("file.docx", 1, 2, 3, 5, 8);
        }
    }
}
