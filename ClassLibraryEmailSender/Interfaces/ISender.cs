﻿namespace ClassLibraryEmailSender.Interfaces
{
    public interface ISender
    {
        void SendMail(string message);
    }
}
