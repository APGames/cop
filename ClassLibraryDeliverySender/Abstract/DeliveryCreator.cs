﻿using ClassLibraryDeliverySender.Interfaces;

namespace ClassLibraryDeliverySender.Abstract
{
    public abstract class DeliveryCreator<T>
    {
        public void SendDeliver(T model)
        {
            var deliver = CreateDeliver();
            deliver.SendDeliver(model);
        }

        public abstract IDeliver<T> CreateDeliver();
    }
}
