﻿using System;
using System.ComponentModel;
using System.Linq;
using Syncfusion.DocIO.DLS;
using Syncfusion.OfficeChart;
using Syncfusion.DocIO;
using System.IO;

namespace ClassLibraryNonVisualComponents
{
    public partial class WordPieChartComponent : Component
    {
        private string[] chartTitles;

        public WordPieChartComponent()
        {
            InitializeComponent();
        }

        public WordPieChartComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public void SetTitles(params string[] titles)
        {
            if (titles.Contains(null))
                throw new NullReferenceException();

            chartTitles = titles;
        }

        public void Save(string fileOutputPath, params double[] chartData)
        {
            if (chartData.Length != chartTitles.Length)
                throw new Exception("Not match lengths datas and titles");

            using (Stream memoryStream = File.OpenWrite(fileOutputPath))
            {
                //Creates a new Word document
                WordDocument document = new WordDocument();
                //Adds section to the document
                IWSection sec = document.AddSection();
                //Adds paragraph to the section
                IWParagraph paragraph = sec.AddParagraph();
                //Inputs data for chart
                object[][] data = new object[chartData.Length + 1][];
                for (int i = 0; i <= chartData.Length; i++)
                    data[i] = new object[2];
                data[0][0] = "";
                data[0][1] = "";
                for (int i = 1; i <= chartData.Length; i++)
                {
                    data[i][0] = chartTitles[i - 1];
                    data[i][1] = chartData[i - 1];
                }
                //Creates and Appends chart to the paragraph
                WChart chart = paragraph.AppendChart(data, 470, 300);
                //Sets chart type and title
                chart.ChartType = OfficeChartType.Pie;
                chart.HasLegend = false;
                chart.ChartTitleArea.FontName = "Calibri";
                chart.ChartTitleArea.Size = 14;
                //Saves the document
                document.Save(memoryStream, FormatType.Docx);
                //Closes the document
                document.Close();
            }
        }
    }
}
