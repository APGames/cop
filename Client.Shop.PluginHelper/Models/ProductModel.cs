﻿using System.Collections.Generic;

namespace Client.Shop.PluginHelper.Models
{
    public class ProductModel
    {
        public string Title { get; set; }
        public int Price { get; set; }
        public string Address { get; set; }
        public int Count { get; set; }
        public List<ClientModel> Subs { get; set; }

        public ProductModel()
        {
            Subs = new List<ClientModel>();
        }
    }
}
