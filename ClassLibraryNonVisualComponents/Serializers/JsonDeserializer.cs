﻿using ClassLibraryNonVisualComponents.Serializers.Interfaces;
using Newtonsoft.Json;

namespace ClassLibraryNonVisualComponents.Serializers
{
    public class JsonDeserializer : IDeserializer
    {
        public T Deserialize<T>(string text)
        {
            return JsonConvert.DeserializeObject<T>(text);
        }
    }
}
