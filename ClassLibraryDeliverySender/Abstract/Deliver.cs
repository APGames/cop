﻿using ClassLibraryDeliverySender.Interfaces;

namespace ClassLibraryDeliverySender.Abstract
{
    public abstract class Deliver<T> : IDeliver<T>
    {
        public bool Used { get; set; }

        public abstract void SendDeliver(T model);
    }
}
